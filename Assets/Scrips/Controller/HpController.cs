﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpController : ProcessController
{
    public delegate void Die();
    public Die die;
    SpriteRenderer aspriteRenderer;

    private void Start()
    {
        currentValue = maxValue;
        aspriteRenderer = transform.GetComponent<SpriteRenderer>();
    }
    public override void SetValue(int newValue)
    {
        base.SetValue(newValue);
        if (currentValue <= 0)
        {
            die();
            return;
        }
        aspriteRenderer.color = Color.Lerp(Color.red, Color.yellow, (float)currentValue / maxValue);
    }
}
