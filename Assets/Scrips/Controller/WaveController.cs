﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LTAUnityBase.Base.DesignPattern;

[System.Serializable]
public class WaveInfor
{
    public int numberEnemy;
    public int levelEnemy;
}

public class WaveController : MonoBehaviour
{
    public Transform[] pointCreateEnemys;
    public WaveInfor[] waves;

    int numberEnemy;
    int currentIndex;

    void Start()
    {
        currentIndex = 0;
        Observer.Instance.AddObserver(TOPTICNAME.ENEMY_DESTROYED, EnemyDie);
        CreateWave();
    }

    void EnemyDie(object data)
    {
        numberEnemy--;
        if (numberEnemy <= 0)
        {
            currentIndex++;
            CreateWave();
        }
    }

    public void CreateWave()
    {
        numberEnemy = waves[currentIndex].numberEnemy;
        WaveInfor currenWave = waves[currentIndex];
        for (int i = 0; i < currenWave.numberEnemy; i++)
        {
            int point = Random.Range(0, pointCreateEnemys.Length);
            Vector3 creatEnemy = new Vector3(pointCreateEnemys[point].position.x, pointCreateEnemys[point].position.y, 0);
            EnemyController enemy = CreatController.Instance.CreatEnemy(creatEnemy);
            enemy.levelController.SetLevel(currenWave.levelEnemy);
        }
        if (currentIndex > waves.Length)
        {
            Debug.Log("You Win!");
            return;
        }
    }
}
public class Waves : SingletonMonoBehaviour<WaveController>
{

}