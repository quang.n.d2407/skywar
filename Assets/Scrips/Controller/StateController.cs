﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LTAUnityBase.Base.DesignPattern;

[System.Serializable]
public class StateInFor
{
    public WaveInfor[] wave;
}

public class StateController : WaveController
{
    public StateInFor[] states;
    int stateIndex;

    void Start()
    {
        stateIndex = 0;
        Observer.Instance.AddObserver(TOPTICNAME.ENEMY_DESTROYED, EndWaves);
        CreateState();
    }

    void EndWaves(object data)
    {
        stateIndex++;
        CreateState();
    }

    void CreateState()
    {
        if (stateIndex >= states.Length)
        {
            Debug.Log("You Win!");
            return;
        }
        StateInFor currentState = states[stateIndex];
        Waves.Instance.waves = currentState.wave;
        Waves.Instance.CreateWave();
    }
}
