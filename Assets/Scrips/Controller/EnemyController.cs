﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LTAUnityBase.Base.DesignPattern;

public class EnemyController : SpaceshipController
{
    float time;
    float count, countRandom;

    float y;
    Vector3 EnemyMove;

    private void Start()
    {
        countRandom = Random.Range(100, 500);
        levelController.SetLevel(1);
    }

    void Update()
    {
        if (time % 200 == 0)
        {
            y = Random.Range(0, -1.5f);
            time = 0;
        }
        EnemyMove = new Vector3(0, y, 0);
        time++;
        Move(EnemyMove);

        //Enemy Shoot
        count++;
        if (count >= countRandom)
        {
            Shoot();
            count = 0;
            countRandom = Random.Range(100, 500);
        }
    }

    protected override void onDestroy()
    {
        Observer.Instance.Notify(TOPTICNAME.ENEMY_DESTROYED, levelController.Level);
        CreatController.Instance.CreatExplotion(this.transform);
        Destroy(gameObject);
    }
}
