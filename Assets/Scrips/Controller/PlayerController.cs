﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LTAUnityBase.Base.DesignPattern;

public class PlayerController : SpaceshipController
{
    float attackDelay;

    protected override void Awake()
    {
        base.Awake();
        levelController.SetLevel(1);
        Observer.Instance.AddObserver(TOPTICNAME.ENEMY_DESTROYED, EnemyDestroyedHandle);
    }

    void Update()
    {
        if (attackDelay % 30 == 0)
        {
            Shoot();
            attackDelay = 0;
        }
        attackDelay++;
    }

    void EnemyDestroyedHandle(object data)
    {
        int enemyLevel = (int)data;
        levelController.SetValue(levelController.currentValue + enemyLevel * 20);
    }

    protected override void onDestroy()
    {
        Destroy(this.gameObject);
    }
}
public class Player : SingletonMonoBehaviour<PlayerController>
{
}
