﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelController : ProcessController
{
    public int level;
    public delegate void LevelUp(int level);
    public LevelUp levelUp;
    public TextMeshPro txtLevel;

    public int Level
    {
        get
        {
            return level;
        }
    }

    void Start()
    {
        SetValue(currentValue);
    }

    public void SetLevel(int newLevel)
    {
        level = newLevel;
        txtLevel.text = "Lv " + level.ToString();
    }

    public override void SetValue(int newValue)
    {
        base.SetValue(newValue);
        if (currentValue >= maxValue)
        {
            SetValue(++level);
            currentValue = 0;
            levelUp(level);
            base.SetValue(currentValue);
            SetLevel(level);
        }
    }
}
