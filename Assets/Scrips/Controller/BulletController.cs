﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MoveController
{
    BulletController bullet;
    //SpaceshipController entity;
    public int count;
    public int damage;
    void Update()
    {
        Move(transform.up);
        if (count == 300)
        {
            Destroy(gameObject);
            // CreatController.Instance.DestroyBullet(bullet);
            return;
        }
        count++;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, 0.2f);
        if (hit.transform != null)
        {
            if (tag != hit.transform.gameObject.tag)
            {
                Ihit ihit = hit.transform.gameObject.GetComponent<Ihit>();
                if (ihit != null)
                {
                    ihit.IHit(damage);
                }
            }
            else return;
            Destroy(gameObject);
        }
    }
}
