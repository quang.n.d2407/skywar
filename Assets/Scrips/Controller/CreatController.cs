﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatController : SingletonMonoBehaviour<CreatController>
{
    public List<BulletController> bullets = new List<BulletController>();
    public BulletController bullet;
    public EnemyController enemy;
    public ExplosionController explosion;

    public BulletController CreatBullet(Transform shootPos)
    {
        if (bullets.Count == 0)
        {
            return Instantiate(bullet, shootPos.position, shootPos.rotation);
        }
        else
        {
            BulletController bullet = bullets[0];
            bullet.count = 0;
            bullet.transform.position = shootPos.position;
            bullet.transform.rotation = shootPos.rotation;
            bullets.Remove(bullet);
            bullet.gameObject.SetActive(true);
            return bullet;
        }
    }

    public EnemyController CreatEnemy(Vector3 enemyPos)
    {
        return Instantiate(enemy, enemyPos, Quaternion.identity);
    }

    public void CreatExplotion(Transform bulletPos)
    {
        Instantiate(explosion, bulletPos.position, bulletPos.rotation);
    }

    public void DestroyBullet(BulletController bullet)
    {
        bullet.gameObject.SetActive(false);
        bullets.Add(bullet);
    }

}
