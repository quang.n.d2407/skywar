﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    public float speed;


    protected virtual void Move(Vector3 direction)
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    #region DragPlayer
    bool isDragged = false;
    Vector3 mouseDragStartPosition;
    Vector3 spriteDragStartPosition;

    private void Awake()
    {
        spriteDragStartPosition = transform.position;
    }

    public void OnMouseDown()
    {
        isDragged = true;
        mouseDragStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        spriteDragStartPosition = transform.localPosition;
    }

    private void OnMouseDrag()
    {
        if (isDragged)
        {
            Vector3 localPos = spriteDragStartPosition + (Camera.main.ScreenToWorldPoint(Input.mousePosition) - mouseDragStartPosition);
            transform.localPosition = new Vector3(localPos.x, localPos.y, 1);
        }
    }

    private void OnMouseUp()
    {
        isDragged = false;
        ResetPosition();
    }

    public void ResetPosition()
    {
        transform.position = spriteDragStartPosition;
    }
    #endregion
}