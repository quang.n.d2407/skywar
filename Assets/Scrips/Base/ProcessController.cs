﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessController : MonoBehaviour
{
    public int currentValue;
    public int maxValue;

    public virtual void SetValue(int newValue)
    {
        currentValue = newValue;
        if (currentValue < 0)
        {
            currentValue = 0;
        }
        else if (currentValue >= maxValue)
        {
            currentValue = maxValue;
        }
        transform.localScale = new Vector3((float)currentValue / maxValue, transform.localScale.y, 1);
    }
}
