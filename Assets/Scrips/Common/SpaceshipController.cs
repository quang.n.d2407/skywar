﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpaceshipController : MoveController, Ihit
{
    public Transform shoot, body;
    public int damage;
    public HpController hpController;
    public LevelController levelController;

    protected virtual void Awake()
    {
        hpController.die = onDestroy;
        levelController.levelUp = UpLevel;
    }

    protected override void Move(Vector3 direction)
    {
        base.Move(direction);
    }

    protected void Shoot()
    {
        CreatController.Instance.CreatBullet(shoot);
    }

    protected abstract void onDestroy();

    public void IHit(int damage)
    {
        hpController.SetValue(hpController.currentValue - damage);
    }
    public void UpLevel(int level)
    {
        damage = level * 5;
        speed = level * 2;
    }
}
